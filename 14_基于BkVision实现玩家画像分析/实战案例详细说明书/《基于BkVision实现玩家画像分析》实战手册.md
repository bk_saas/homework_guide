# bk-vision-saas

## 介绍
结合蓝鲸体系和开发蓝鲸框架，实现基于BkVision实现玩家画像分析。
- 内容
  - 了解bk-vision产品功能， 
  - 掌握bk-vision报表SDK的集成，对业务运营数据进行图标分析展示和运营决策
- 目标
  - 采集服务端运营数据（玩家画像分析： 登陆，游戏情况）
  - 利用bk-vision的SDK做运营视图报表

## 软件架构
采用Python3，基于Django默认引擎/mako模板引擎的前后端不分离模式。

## 环境搭建
参考[《腾讯蓝鲸企业级PaaS解决方案》实战手册](./../1_企业级PaaS解决方案/《腾讯蓝鲸企业级PaaS解决方案》实战手册.md)

## 实现步骤
1. #####基于之前的实验项目，添加中间件去捕获用户行为，并记录。
    - 创建操作日志数据模型
    ```python
     class Records(models.Model):
         operator = models.CharField(verbose_name="操作人", max_length=128)
         operate_time = models.DateTimeField(auto_now_add=True, verbose_name="操作时间")
         operate_action = models.CharField(verbose_name="操作内容", max_length=255)
         operate_status = models.CharField(verbose_name="操作结果", max_length=255)
         create_time = models.DateTimeField(verbose_name="日志创建时间", auto_now_add=True)
         update_time = models.DateTimeField(verbose_name="日志更新时间", auto_now=True)
         is_deleted = models.BooleanField(verbose_name="逻辑删除", default=False)
         input_params = models.JSONField(null=True, verbose_name="请求参数")
         output_params = models.JSONField(null=True, verbose_name="输出参数")
     ```
   - 生成模型对应的migration文件，并应用于mysql数据库
   ```shell script
    python manage.py makemigrations
    python manage.py migrate
   ```
   
   - 在项目应用目录下创建中间件middleware.py
   
   ![bkvision1.png](./img/bkvision1.png)
   ![bkvision2.png](./img/bkvision2.png)
   
   - 在config/default.py注册中间件
   ![bkvision15.png](./img/bkvision15.png)
   
2. ##### 接入项目的数据库到BKVision做为数据源，并配置仪表盘。
    - 在蓝鲸开发者中心获取数据库信息
    ![bkvision3.png](./img/bkvision3.png)
    - 在BKVision平台https://apps.ce.bktencent.com/bk-vision/接入数据源
    ![bkvision4.png](./img/bkvision4.png)
    - 配置仪表盘
    ![bkvision5.png](./img/bkvision5.png)
    ![bkvision6.png](./img/bkvision6.png)
3. ##### 在BKVision创建仪表盘分享嵌入。
    - 创建嵌入
    ![bkvision7.png](./img/bkvision7.png)
    - 选择嵌入类型（iFrame, JS SDK）
    ![bkvision8.png](./img/bkvision8.png)
    - 获取嵌入代码
    ![bkvision9.png](./img/bkvision9.png)
4. ##### 在自身项目内使用BKVision-SDK注册路由, 并用嵌入代码构建前端页面。
    - 复制BKVision-SDK[(https://gitee.com/bk_saas/bkvision-sdk)](https://gitee.com/bk_saas/bkvision-sdk)到项目根目录
    ![bkvision10.png](./img/bkvision10.png)
    - 注册SDK接口路由
    ![bkvision11.png](./img/bkvision11.png)
    - 添加tab页用于嵌入bkvision仪表盘
    ![bkvision20.png](./img/bkvision20.png)
    - 使用嵌入代码构建前端页面（iFrame）
    - iFrame需注意：嵌入的url建议使用https开头，本地调试时会出现url连接被拒绝的情况，部署到线上就OK了。
    ![bkvision12.png](./img/bkvision12.png)
    - 使用嵌入代码构建前端页面（JS SDK）
    ![bkvision13.png](./img/bkvision13.png)
    ![bkvision16.png](./img/bkvision16.png)
    ![bkvision17.png](./img/bkvision17.png)
    - JS SDK会覆盖已有组件的样式，需要手动调整一下已有的页面样式
    ![bkvision18.png](./img/bkvision18.png)
    ![bkvision19.png](./img/bkvision19.png)
5. ##### 完成运营数据仪表盘的嵌入。
    ![bkvision14.png](./img/bkvision14.png)
