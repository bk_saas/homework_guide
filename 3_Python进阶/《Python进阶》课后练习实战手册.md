## 作业一：Python类的编写与继承

### 要求

编写以下类，并创建 `Student` 和 `CollegeStudent` 对象进行测试，让子类调用自身和父类的方法。

- 编写 `Person` 类，具有以下属性和方法：
  1. 属性：姓名 `name`, 年龄 `age` 
  2. 方法：`speak()` ，打印出当前 `Person` 对象的姓名和年龄

- 编写 `Student` 类继承 `Person`类，具有以下属性和方法：
  1. 属性：姓名 `name`, 年龄 `age`, 学校 `school` 和 学号 `my_id`
  2. 方法：`speak()`，打印出当前 `Student` 对象的所有属性信息
  3. 方法：`run (v,  times)`，设置学生速度和时间，输出学生跑步距离

- 编写 `CollegeStudent` 类继承 `Student` 类，具有以下属性和方法：
  1. 属性：姓名  `name`, 年龄 `age`, 学校 `school`, 学号 `my_id` 和 语言 `language`
  2. 方法：`speak()`，打印出当前 `CollegeStudent` 对象的所有属性信息
  3. 方法：`coding()`，使用10秒钟打印出 `"I am coding .........."`（十个点）



### 参考代码

```python
from time import sleep


class Person:
    """Person类"""
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def speak(self):
        print("我是%s, 今年%s岁了。" % (self.name, self.age))


class Student(Person):
    """Student类"""
    def __init__(self, name, age, school, id):
        super(Student, self).__init__(name, age)
        self.school = school
        self.my_id = id

    def speak(self):
        print("我是%s, 今年%s岁了, 我来自%s, 学号是%d。"
              % (self.name, self.age, self.school, self.my_id))

    def run(self, v, times):
        return v*times


class CollegeStudent(Student):
    def __init__(self, name, age, school, id, language):
        super(CollegeStudent, self).__init__(name, age, school, id)
        self.language = language

    def speak(self):
        print("我是%s, 今年%s岁了, 我来自%s, 学号是%d，我会%s。"
              % (self.name, self.age, self.school, self.my_id, self.language))

    def coding(self):
        print("I am coding", end=" ")
        for i in range(10):
            print(".", end="")
            sleep(1)


x = CollegeStudent('Jim', 18, 'Blueking', 200101, 'English')
x.speak()
x.coding()

```



## 作业二：定义一个授权装饰器函数并测试其功能

### 要求

1. 编写一个 `AuthClass` 类封装 `name` 和 `pwd` 属性，对应用户名和密码类中拥有授权方法 `auth()` ，判断当前对象的 `name` 值是否为`python`, `pwd`值是否为 `123`，如果是返回 `True`，否则返回 `False`。
2. 基于 `AuthClass` 创建一个用户对象 `user`，创建时填入用户名和密码
3. 编写装饰器函数，首先让 `user` 对象来验证身份，如果验证通过，运行被装饰的函数，否则打印认证失败，返回 `False`



### 参考代码

```python
from functools import wraps

class AuthClass:
    def __init__(self, name, pwd):
        self.name = name
        self.pwd = pwd
    
    def auth(self):
        if self.name == "python" and self.pwd == "123":
            return True
        else:
            return False

user = AuthClass("python", "1123")

def auth_before(f):
    @wraps(f)
    def inner(*args, **kwargs):
        if not user.auth():
            print("用户名或密码错误")
            return False
        else:
            return f(*args, **kwargs)
    return inner

@auth_before
def p1():
    return "this is p1 function"

print(p1())

```



## 作业三：定义一个带文件名参数的记录日志装饰器函数并测试其功能

### 要求

1. 在被装饰函数运行之前，在屏幕打印运行函数的名称，并记录当前系统时间

2. 给装饰器函数增加输入日志文件名参数，在日志记录时将日志信息写入到对应的日志文件



```python
from datetime import datetime
from functools import wraps


def log_it(logfile='out.log'):
    def logging_decorator(func):
        @wraps(func)
        def wrapped_function(*args, **kwargs):
            log = f"[{datetime.now()}] {func.__name__} was called"
            # 打开logfile，并写入内容
            with open(logfile, 'a') as fd:
                # 现在将日志打到指定的logfile
                fd.write(log + '\n')
            return func(*args, **kwargs)
        return wrapped_function
    return logging_decorator


@log_it()
def my_func1():
    pass


@log_it(logfile='func2.log')
def my_func2():
    pass


my_func1()
my_func2()

```

