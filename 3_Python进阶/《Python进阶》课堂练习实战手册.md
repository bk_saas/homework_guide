## 例1：p4

```python
class MyClass:
    """一个简单的类实例"""
    my_name = 'python'

    def a_fun(self):
        return 'welcome '+ self.my_name


# 实例化类
x = MyClass()
# 访问类的属性和方法
print("我的名字是", x.my_name)
print("方法 a_fun 输出为：", x.a_fun())
print(x.__class__)
print("类的属性值为：", MyClass.my_name)
```



## 例2：p5

```python
>>>print(dir(1))
>>>print(dir(print))
>>>print(dir(['a',1]))
```



## 例3：p7

```python
class MyClass:
    """一个简单的类实例"""
    my_name = 'python'

    def __init__(self, my_school):
        self.my_school = my_school

    def a_fun(self):
        return 'welcome ' + self.my_name


# 实例化类
x = MyClass('tencent')
# 访问类的属性和方法
print("我的名字是", x.my_name)
print("方法 a_fun 输出为：", x.a_fun())
print(x.__class__)
print('类的属性值为：', MyClass.my_name)
print(type(x), dir(x))
print(MyClass.my_name)
print(MyClass.my_school) # 错误，没有my_school变量
```



## 例4：p8

```python
class People:

    # 定义构造方法
    def __init__(self, n=None, a=0, w=0):
        self.name = n
        self.age = a
        self.weight = w

    def speak(self):
        print("%s 说: 我 %d 岁。" % (self.name, self.age))


# 单继承示例
class Student(People):

    def __init__(self, n, a, w, g):
        self.name = n
        self.age = a
        self.weight = w
        self.grade = g

    def talk(self):
        print("%s 说: 我 %d 岁了，我在读 %d 年级" % (self.name, self.age, self.grade))


s = Student('ken', 10, 60, 3)
s.speak()
s.talk()

```



## 例5：p9

```python
class People:

    # 定义构造方法
    def __init__(self, n=None, a=0, w=0):
        self.name = n
        self.age = a
        self.weight = w

    def speak(self):
        print("%s 说: 我 %d 岁。" % (self.name, self.age))


# 单继承示例
class Student(People):
    grade = ''

    def __init__(self, n, a, w, g):
        # 调用父类的构函
        super(Student, self).__init__(n, a, w)
        self.grade = g

    # 覆写父类的方法
    def speak(self):
        print("%s 说: 我 %d 岁了，我在读 %d 年级" % (self.name, self.age, self.grade))


s = Student('ken', 10, 60, 3)
s.speak()
super(Student,s).speak()

```





## 例6：p10 

加入私有属性

```python
class People:

    # 定义构造方法
    def __init__(self, n=None, a=0, w=0):
        self.name = n
        self.age = a
        self.weight = w

    def speak(self):
        print("%s 说: 我 %d 岁。" % (self.name, self.age))


# 单继承示例
class Student(People):

    def __init__(self, n, a, w, g):
        super(Student, self).__init__(n, a, w)
        self.__grade = g

    # 覆写父类的方法
    def __speak(self):
        print("%s 说: 我 %d 岁了，我在读 %d 年级" % (self.name, self.age, self.__grade))

    def talk(self):
        self.__speak()


s = Student('ken', 10, 60, 3)
s.speak()
# s.__speak()
s.talk()
# print(s.__grade)

```



## 例7：p13 

定义两个向量的加法

```python
class Vector:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __repr__(self):
        return 'I am a Vector (%d, %d)' % (self.a, self.b)

    def __str__(self):
        return 'Vector (%d, %d)' % (self.a, self.b)

    def __add__(self, other):
        return Vector(self.a + other.a, self.b + other.b)

print(dir(Vector))
v1 = Vector(2, 10)
v2 = Vector(5, -2)
print(v1 + v2)
print(repr(v1))
```



## 例8：p14 

课堂练习一：定义一个类描述时钟计时

```python
from time import sleep


class Clock(object):
    """数字时钟"""

    def __init__(self, hour=0, minute=0, second=0):
        """初始化方法
        :param hour: 时
        :param minute: 分
        :param second: 秒
        """
        self.hour = hour
        self.minute = minute
        self.second = second

    def run(self):
        """走字"""
        self.second += 1
        if self.second == 60:
            self.second = 0
            self.minute += 1
            if self.minute == 60:
                self.minute = 0
                self.hour += 1
                if self.hour == 24:
                    self.hour = 0

    def show(self):
        """显示时间"""
        return '%02d:%02d:%02d' % \
               (self.hour, self.minute, self.second)


def main():
    clock = Clock(23, 59, 58)
    while True:
        print(clock.show())
        sleep(1)
        clock.run()


if __name__ == '__main__':
    main()

```



## 例9：p14

课堂练习二：定义一个类描述平面上的点并提供移动点和计算到另一个点距离的方法

```python
from math import sqrt


class Point(object):

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def move_to(self, x, y):
        self.x = x
        self.y = y

    def distance_to(self, other):
        dx = self.x - other.x
        dy = self.y - other.y
        return sqrt(dx ** 2 + dy ** 2)

    def __str__(self):
        return '(%s, %s)' % (str(self.x), str(self.y))


def main():
    p1 = Point(3, 5)
    p2 = Point()
    print(p1)
    print(p2)
    p2.move_to(-1, 2)
    print(p2)
    print(p1.distance_to(p2))


if __name__ == '__main__':
    main()

```



##  例10：p28 

定义一个日志装饰器

```python
from functools import wraps
import datetime


def log_it(func):
    @wraps(func)
    def with_logging(*args, **kwargs):
        print(func.__name__, " was called at", datetime.datetime.now())
        return func(*args, **kwargs)
    return with_logging


@log_it
def addition_func(x):
    return x + x


result = addition_func(4)
print(result)
from functools import wraps

def logit(logfile='out.log'):
    def logging_decorator(func):
        @wraps(func)
        def wrapped_function(*args, **kwargs):
            log_string = func.__name__ + " was called"
            print(log_string)
            # 打开logfile，并写入内容
            with open(logfile, 'a') as opened_file:
                # 现在将日志打到指定的logfile
                opened_file.write(log_string + '\n')
            return func(*args, **kwargs)

        return wrapped_function

    return logging_decorator

@logit()
def myfunc1():
    pass

myfunc1()

@logit(logfile='func2.log')
def myfunc2():
    pass

myfunc2()
```



## 例11：p35 

定义一个异常处理装饰器

```python
from functools import wraps


def exp_test(fun):
    @wraps(fun)
    def wrapped_function(*args, **kwargs):
        try:
            fun(*args, **kwargs)
            print("function goes well...")
        except BaseException as errors:
            print("function has an errors...")
            print(errors)
        finally:
            print("function end with logs")

    return wrapped_function


@exp_test
def try_test():
    # x = 0/1
    x = 1/0


if __name__ == '__main__':
    try_test()

```



## 例12：p40 

实现一个网络爬虫

```python
import requests
from bs4 import BeautifulSoup

url = "https://s.weibo.com/top/summary"  # 微博热搜
web_html = requests.get(url, timeout=30)

print(web_html.status_code)  # 打印状态码 --- 200
print(web_html.headers)  # 打印头信息
# print(web_html.content)  # 以字节的方式显示，中文显示为字符形式
# print(web_html.text)  # 以text的方式显示
print(web_html.url)  # url地址
print(web_html.encoding)  # 编码格式

# 声明bs对象和解析器，返回解析后的网页信息
bs4 = BeautifulSoup(web_html.content, 'lxml')

# 直接取文本，更简洁，属于字符串形式，看上去比较零散，因为有很多'\n'
# print(bs4.get_text())

# 格式化代码，对齐、缩进、换行等
# print(bs4.prettify())
'''
out_str = bs4.prettify()
with open('test_html.html', 'w', encoding='utf-8') as f:
    f.write(out_str)

# 直接标签内容
print('title内容：\n', bs4.title.string)  # 以string格式打印出title标签中的内容
print('title标签：\n', bs4.title)  # 返回标题值 <title></title>之间的值，因为一般只会有一个title
'''

timehot_rank_list = []
timehot_href_list = []
timehot_content_list = []
timehot_num_list = []
timehot_div = bs4.find("div", {'class': "data", 'id': "pl_top_realtimehot"})  # 如果是字典形式，则key要与html文件中的一致
# timehot_tbody = timehot_div.find("tbody").get_text()   #获取文本形式的数据

# timehot_tbody = timehot_div.find("tbody")   #可用
timehot_tbody = timehot_div.tbody  # 返回第一个tbody，如果只有一个tbody，也可以直接用
return_str = ''
for ii, mm in enumerate(timehot_tbody.find_all("tr")):
    rank = mm.find("td", class_="td-01 ranktop")
    td = mm.find("td", class_="td-02")
    timehot_href_list.append(td.a['href'])
    timehot_content_list.append(td.a.string)

    if ii == 0:
        timehot_rank_list.append('0')
        timehot_num_list.append('9999999999')
        return_str = return_str + '\t' + '0' + '\t' + td.a.string + '\t' + td.a['href'] + '\t' + '9999999999' + '\n'
    else:
        timehot_rank_list.append(rank.string)
        timehot_num_list.append(td.span.string)
        return_str = return_str + '\t' + rank.string + '\t' + td.a.string + '\t' + td.a[
            'href'] + '\t' + td.span.string + '\n'

with open('微博热搜榜.txt', 'w', encoding='utf-8') as f:
    f.write(return_str)
```



## 例13：p41 

读excel文件内容

```python
import xlrd
import exception_test
from datetime import datetime, date

@exception_test.exp_test
def file_read(filename='test.xls'):
    # 打开Excel文件读取数据
    data = xlrd.open_workbook(filename)
    sheet_name = data.sheet_names()  # 获取所有sheet名称
    print(sheet_name)  # ['学生']

    # 根据下标获取sheet名称
    sheet2_name = data.sheet_names()[0]
    print(sheet2_name)  # 学生

    # 根据sheet索引或者名称获取sheet内容，同时获取sheet名称、列数、行数
    sheet2 = data.sheet_by_index(0)
    print('sheet2名称:{}\nsheet2列数: {}\nsheet2行数: {}'
          .format(sheet2.name, sheet2.ncols, sheet2.nrows))

    #  根据sheet名称获取整行和整列的值
    sheet1 = data.sheet_by_name('学生')
    # 获取某行信息
    print(sheet1.row_values(3))
    # 获取某列信息
    print(sheet1.col_values(3))
    # 获取指定单元格的内容
    print(sheet1.cell(1, 0).value)  # 第2 行1列内容
    print(sheet1.cell_value(1, 0))  # 第2 行1列内容
    print(sheet1.row(1)[0].value)  # 第2 行1列内容
    # 获取单元格内容的数据类型
    print(sheet1.cell(1, 0).ctype)  # 第2 行1列内容 ：1
    print(sheet1.cell(3, 3).ctype)  # 第4行5列内容：0
    print(sheet1.cell(6, 1).ctype)  # 第4 行7列内容：1
    # 说明 ctype : 0 empty, 1 string, 2 number, 3 date, 4 boolean, 5 error
    # 使用xlrd的xldate_as_tuple处理为date格式
    if sheet1.cell(2, 5).ctype == 3:
        print(sheet1.cell(2, 5).value)  #
        date_value = xlrd.xldate_as_tuple(sheet1.cell(2, 5).value, data.datemode)
        print(date_value)  # (2020, 10, 20, 0, 0, 0)
        print(date(*date_value[:3]))  # 2020-10-20
        print(date(*date_value[:3]).strftime('%Y/%m/%d'))  # 2020/10/20

    # 获取合并单元格内容
    # 这里，需要在读取文件的时候添加个参数，将formatting_info参数设置为True，默认是False，否
    # 则可能调用merged_cells属性获取到的是空值。
    data = xlrd.open_workbook(filename, formatting_info=True)
    sheet1 = data.sheet_by_name('学生')
    print(sheet1.merged_cells)  # [(3, 5, 3, 4)]
    # merged_cells返回的这四个参数的含义是：(row,row_range,col,col_range),其中[row,row_range)包括row,
    # 不包括row_range,col也是一样，下标从0开始。
    # (3, 5, 3, 4) 表示列[3,4)的[3,5)行合并,即第3列，3和4行合并
    # 获取合并个单元格的内容：
    print(sheet1.cell(3, 3).value)  # 打篮球

    # 更加方便的方式来获取合并单元格内容
    merge_value = []
    for (row, row_range, col, col_range) in sheet1.merged_cells:
        merge_value.append((row, col))

    print(merge_value)  # [(3, 3)]
    for v in merge_value:
        print(sheet1.cell(v[0], v[1]).value)

if __name__ == '__main__':
    file_read()
```



## 例14：p41 

写入excel文件内容

```python
import xlwt
from datetime import datetime, date

def set_style(name, height, bold=False, format_str=''):
    style = xlwt.XFStyle()  # 初始化样式

    font = xlwt.Font()  # 为样式创建字体
    font.name = name  # 'Times New Roman'
    font.bold = bold
    font.height = height

    borders = xlwt.Borders()  # 为样式创建边框
    borders.left = 6
    borders.right = 6
    borders.top = 6
    borders.bottom = 6

    style.font = font
    style.borders = borders
    style.num_format_str = format_str

    return style

# 创建一个excel文件对象
wb = xlwt.Workbook()

# 以下是基本写入方法
sheet1 = wb.add_sheet('学生', cell_overwrite_ok=True)
row0 = ["姓名", "年龄", "出生日期", "爱好"]
colum0 = ["张三", "李四", "恋习Python", "小明", "小红", "无名"]
# 写第一行
for i in range(0, len(row0)):
    sheet1.write(0, i, row0[i], set_style('Times New Roman', 220, True))
# 写第一列
for i in range(0, len(colum0)):
    sheet1.write(i + 1, 0, colum0[i], set_style('Times New Roman', 220, True))
    sheet1.write(1, 3, '2006/12/12')
    sheet1.write_merge(6, 6, 1, 3, '未知')  # 合并行单元格
    sheet1.write_merge(1, 2, 3, 3, '打游戏')  # 合并列单元格
    sheet1.write_merge(4, 5, 3, 3, '打篮球')

wb.save('test.xls')  # 保存到test.xls

#以下是带格式写入
ws = wb.add_sheet('A Test Sheet')  # 增加sheet
ws.col(0).width = 200 * 30  # 设置第一列列宽

ws.write(0, 0, 1234.56, set_style('Times New Roman', 220, bold=True, format_str='#,##0.00'))
ws.write(1, 0, datetime.now(), set_style('Times New Roman', 220, bold=False, format_str='DD-MM-YYYY'))
styleOK = xlwt.easyxf('pattern: fore_colour light_blue;'
                      'font: colour green, bold True;')

# 设置表格样式
pattern = xlwt.Pattern()  # 一个实例化的样式类
pattern.pattern = xlwt.Pattern.SOLID_PATTERN  # 固定的样式
pattern.pattern_fore_colour = xlwt.Style.colour_map['red']  # 背景颜色
styleOK.pattern = pattern
ws.write(2, 0, 1, style=styleOK)
ws.write(2, 1, 1)
ws.write(2, 2, xlwt.Formula("A3+B3"))

wb.save('example.xls')  # 保存xls

```



## 例15：p42 

zip文件操作

```python
import zipfile
import os

# 读取或创建zip文件
z = zipfile.ZipFile('E:\\教学相关\\2020腾讯大数据\\1.zip', 'r')
# 这里的第二个参数用r表示是读取zip文件，w是创建一个zip文件
# 获取zip中文件名列表
for f in z.namelist():
    print(f)
# 获取zip内所有文件信息
for i in z.infolist():
    print(i.filename, i.file_size)
# 解压缩并读取zip中第二个文件，可以再存储到文件。
print(z.read(z.namelist()[1]))

# 创建zip压缩包的方法，与读取的方法类似
z1 = zipfile.ZipFile('2.zip', 'w')

# 把'D:\pyProject'中的文件全部添加到压缩包里（这里只添加一级子目录中的文件）：
test_dir = r'D:\pyProject'
if os.path.isdir(test_dir):
    for d in os.listdir(test_dir):
        z1.write(test_dir + os.sep + d)
        # close() 是必须调用的！
    z1.close()

```



## 例16：p43

```python
import shutil

# 将文件func2.log的内容拷贝至out.log
f1 = open('func2.log','r')
f2 = open('out.log','w')
shutil.copyfileobj(f1, f2, length=16*1024)

# 也可使用创建一个空的目标文件，原文件copy进去
# shutil.copyfile('src', 'dst')
# 仅拷贝文件权限
shutil.copymode('func2.log', 'out.log')
# 当前目录下创建压缩文件'project_a.zip',包含内容为'D:\pyProject\temp'下文件
ret = shutil.make_archive("project_a", 'zip', base_dir=r'D:\pyProject\temp')

f2.close()
# 移动文件，从源'out.log'复制到'new_out.log'
shutil.move('out.log', r'D:\pyProject\temp\new_out.log')

```

