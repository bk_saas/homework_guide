# 基于监控平台进行告警分析

## 介绍
结合蓝鲸体系和开发蓝鲸框架，开发一个小型游戏业务管理SaaS应用。该应用需要先获取CMDB业务/拓扑/主机相关信息，然后通过调用告警接口展示并分析相关告警。

## 软件架构

采用Python3，基于Django默认引擎/mako模板引擎的前后端不分离模式。

## 功能特性

基于监控平台进行告警分析

### 内容

1. 了解监控平台的基本概念
2. 熟悉掌握监控平台 `告警查询` 接口的调用

### 目标

1. 监控平台的基本概念和产品使用介绍
2. 如何通过SaaS应用调用监控平台接口
3. 实验作业考察：调用配置平台、监控平台相关API，实现一个小型游戏业务告警分析SaaS应用

## 实战步骤

### 1. 环境搭建搭建  

- 开发环境搭建参考 [《腾讯蓝鲸企业级PaaS解决方案》实战手册](https://gitee.com/bk_saas/homework_guide/blob/master/1_%E4%BC%81%E4%B8%9A%E7%BA%A7PaaS%E8%A7%A3%E5%86%B3%E6%96%B9%E6%A1%88/%E3%80%8A%E8%85%BE%E8%AE%AF%E8%93%9D%E9%B2%B8%E4%BC%81%E4%B8%9A%E7%BA%A7PaaS%E8%A7%A3%E5%86%B3%E6%96%B9%E6%A1%88%E3%80%8B%E5%AE%9E%E6%88%98%E6%89%8B%E5%86%8C.md)

- 在settings.py内配置 `BK_BIZ_ID = 3`

### 2. 修改前端页面

使用 **核心源码** 中的 `frontend/index_home.html` 
替换 `home_application/templates/home_application/index_home.html`

### 3. 查询告警接口

> 告警查询接口文档: https://apigw.ce.bktencent.com/docs/component-api/default/MONITOR_V3/search_alert/doc

检查 `blueking/component/apis/monitor_v3.py` 中的接口，如果缺少search_alert接口，则增加search_alert接口

```python
self.search_alert = ComponentAPI(
    client=self.client, method='POST',
    path='/api/c/compapi{bk_api_ver}/monitor_v3/search_alert/',
    description=u'查询告警信息'
)
```

增加 `home_application/tasks.py` ，新增查询告警函数

1. 查询最近一天的告警数据
2. 分页拉取，先拉取第一页数据，根据返回的 `total` 再拉取剩余页数据

单条alert示例：
```json
{
  "id": "168387970514723",
  "alert_name": "测试推送到FlashDuty_copy",
  "status": "ABNORMAL",
  "description": "REAL_TIME(CPU使用率) >= 0.0%, 当前值0.700467%",
  "severity": 1,
  "metric": ["bk_monitor.system.cpu_summary.usage"],
  "bk_biz_id": 3,
  "ip": "10.0.48.18",
  "bk_cloud_id": 0,
  "bk_service_instance_id": null,
  "bk_topo_node": [
    "module|78",
    "set|18",
    "biz|3",
    "environment|3",
    "subsystem|6"
  ],
  "assignee": ["admin"],
  "appointee": null,
  "is_ack": null,
  "is_shielded": false,
  "shield_left_time": "0s",
  "shield_id": null,
  "is_handled": true,
  "strategy_id": 169,
  "create_time": 1683879705,
  "update_time": 1683981142,
  "begin_time": 1683877676,
  "end_time": null,
  "latest_time": 1683981116,
  "first_anomaly_time": 1683877676,
  "target_type": "HOST",
  "target": "10.0.48.18|0",
  "category": "os",
  "tags": [],
  "category_display": "主机&云平台-操作系统",
  "duration": "1d 4h",
  "ack_duration": null,
  "data_type": "time_series",
  "converge_id": "168387970514723",
  "event_id": "424e454d6dc5eda67d717e45821f847b.1683981116.169.169.1",
  "plugin_id": "bkmonitor",
  "stage_display": "已通知",
  "dimensions": [
    {
      "display_value": "10.0.48.18",
      "display_key": "目标IP",
      "value": "10.0.48.18",
      "key": "ip"
    },
    {
      "display_value": 0,
      "display_key": "云区域ID",
      "value": 0,
      "key": "bk_cloud_id"
    }
  ],
  "seq_id": 14723,
  "dedupe_md5": "800b63a023dbf885e6532f14d7024c52",
  "dedupe_keys": [
    "strategy_id",
    "target_type",
    "target",
    "bk_biz_id"
  ],
  "dimension_message": "目标IP(10.0.48.18)",
  "metric_display": [
    {
      "id": "bk_monitor.system.cpu_summary.usage",
      "name": "CPU使用率"
    }
  ],
  "target_key": "主机 10.0.48.18",
  "ack_operator": "",
  "shield_operator": [],
  "strategy_name": "测试推送到FlashDuty_copy",
  "bk_biz_name": "demo体验业务"
}
```

![img.png](img/img.png)

### 4. 新增告警数据存储模型

在 `home_application/models.py`，新增告警数据存储模型

1. 将alert_id设置为唯一键，防止重复数据
2. 有些字段可能为null，需要设置为null=True

![img.png](img/img1.png)

### 5. 将接口返回的告警数据存储到数据库

> 在后台任务中调用接口时缺少人员登录态，需要开通特殊的调用权限，如果发现接口调用提示无权限，请提供自己的app_code联系老师帮忙开通

在 `home_application/tasks.py`，新增将接口返回的告警数据存储到数据库的函数

![img.png](img/img2.png)

在 `home_application/tasks.py`，新增celery异步定时任务

![img.png](img/img6.png)

示例中的 `sync_alert_monitor_data` 任务配置默认为每分钟执行一次，可以根据自己的需求修改

配置项目Celery配置，启动celery worker和celery beat

在 `config/default.py` 修改配置，就改后需要再次执行 `python manage.py migrate` 迁移celery相关数据表

```python
# 启用celery
IS_USE_CELERY = True

# 禁用celery时区设置
CELERY_ENABLE_UTC = False
DJANGO_CELERY_BEAT_TZ_AWARE = False
```

在 local_settings.py 中配置broker_url，下面分别是RabbitMQ和Redis的配置，根据自己的情况选择

```python
# Celery 消息队列设置 RabbitMQ
BROKER_URL = 'amqp://guest:guest@localhost:5672//'

# Celery 消息队列设置 Redis
BROKER_URL = "redis://localhost:6379/0"
```

启动celery worker和celery beat

```shell
celery -A config worker -l info
celery -A config beat -l info
```

为了方便调试，也可以添加临时接口用于主动触发任务

```python
# home_application/views.py
def refresh_alert_data(request):
    """
    刷新告警数据
    """
    data = sync_monitor_alert_data(request)
    return JsonResponse({
        "result": True,
        "data": data,
    })

# home_application/urls.py
urlpatterns = [
    ...
    url(r"^refresh_alert_data/", views.refresh_alert_data, name="刷新告警数据"),
]
```

### 6. 新增告警数据查询接口

在 `home_application/views.py`，新增告警数据查询接口

```python
# 告警级别
ALERT_SEVERITY = {1: "致命", 2: "预警", 3: "提醒"}

# 告警状态
ALERT_STATUS = {"RECOVERED": "已恢复", "CLOSED": "已关闭", "ABNORMAL": "未恢复"}
```

![img.png](img/img3.png)

在 `home_application/urls.py`，新增告警数据查询接口的路由

```python
urlpatterns = [
    ...
    url(r"^get_alert_monitor/", views.get_alert_monitor, name="告警数据"),
]
```

### 7. 新增告警数据统计接口

在 `home_application/views.py`，新增告警数据统计接口

![img.png](img/img4.png)

在 `home_application/urls.py`，新增告警数据统计接口的路由

```python
urlpatterns = [
    ...
    url(r"^get_alert_monitor_group_data/", views.get_alert_monitor_group_data, name="可视化告警数据"),
]
```

### 8. 上线部署

开发框架初始化的配置里面默认只有web进程，我们需要添加celery相关的进程启动配置

调整 `app_desc.yaml` 中的配置，增加 `worker` 和 `beat` 进程
```yaml
spec_version: 2
module:
  language: Python
  scripts:
    pre_release_hook: "python manage.py migrate --no-input"
  processes:
    web:
      command: gunicorn wsgi -w 4 -b :$PORT --access-logfile - --error-logfile - --access-logformat '[%(h)s] %({request_id}i)s %(u)s %(t)s "%(r)s" %(s)s %(D)s %(b)s "%(f)s" "%(a)s"'
    worker:
      command: celery worker -A config -l info
    beat:
      command: celery beat -A config -l info
```

在项目部署前，还需要在`开发者中心-数据存储`手动启用rabbitmq组件

![img.png](img/img5.png)



## 参考资料
1. 监控平台访问地址: https://bkmonitor.ce.bktencent.com/
2. 监控告警数据查询接口: https://apigw.ce.bktencent.com/docs/component-api/default/MONITOR_V3/search_alert/doc
