### url配置

1. 修改 `urls.py` 文件

   ```python
   from django.conf.urls import include, url
   from django.contrib import admin
   
   urlpatterns = [
       url(r'^admin/', admin.site.urls),
       url(r'^account/', include('blueapps.account.urls')),
       # 如果你习惯使用 Django 模板，请在 home_application 里开发你的应用，
       # 这里的 home_application 可以改成你想要的名字
       url(r'^', include('award_declaration.urls')),
       url(r'^', include('home_application.urls')),
       # 如果你习惯使用 mako 模板，请在 mako_application 里开发你的应用，
       # 这里的 mako_application 可以改成你想要的名字
       url(r'^mako/', include('mako_application.urls')),
       url(r'^i18n/', include('django.conf.urls.i18n'))
   ]
   
   ```

   

2. 修改 `home_application/urls.py` 文件这三处地方
   ![image-20201116151613921](demo作业和奖项申报作业url配置.assets/image-20201116151613921.png)

   ```python
   from django.conf.urls import url
   
   from . import views
   from blueapps.account.views import get_user_info
   
   urlpatterns = (
       url(r'^frontend/$', views.home),
       url(r'^dev-guide/$', views.dev_guide),
       url(r'^contact/$', views.contact),
       url(r'^helloworld/$', views.helloworld),
       url(r'^python/homework1/$', views.homework1),
       url(r'^python/homework2/$', views.homework2),
       url(r'^python/homework3/$', views.homework3),
       url(r'^frontend/account/get_user_info/$', get_user_info, name="get_user_info")
   )
   
   ```

   

