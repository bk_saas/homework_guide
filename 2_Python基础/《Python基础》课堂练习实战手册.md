# 《Python基础》课堂练习实战手册



### 练习一

![image-20201016144306522](imgs/image-20201016144306522.png)



http://www.pythonchallenge.com/pc/def/map.html

解码如下文本

```
g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj.
```



### 练习二

![image-20201016144542284](imgs/image-20201016144542284.png)

请注意

- 品牌由多个单词组成时，只输出第一个单词
- 颜色字段统一输出 `black`

其中，`shoes.txt` 的内容为以下，请在本地创建该文件

```
Adidas  orange  43
Nike    black   41
Adidas  black   39
New Balance pink    41
Nike    white   44
New Balance orange  38
Nike    pink    44
Adidas  pink    44
New Balance orange  39
New Balance black   43
New Balance orange  44
Nike    black   41
Adidas  orange  37
Adidas  black   38
Adidas  pink    41
Adidas  white   36
Adidas  orange  36
Nike    pink    41
Adidas  pink    35
New Balance orange  37
Nike    pink    43
Nike    black   43
Nike    black   42
Nike    black   35
Adidas  black   41
New Balance pink    40
Adidas  white   35
New Balance pink    41
New Balance orange  41
Adidas  orange  40
New Balance orange  40
New Balance white   44
New Balance pink    40
Nike    black   43
Nike    pink    36
New Balance white   39
Nike    black   42
Adidas  black   41
New Balance orange  40
New Balance black   40
Nike    white   37
Adidas  black   39
Adidas  black   40
Adidas  orange  38
New Balance orange  39
Nike    black   35
Adidas  white   39
Nike    white   37
Adidas  orange  37
Adidas  pink    35
New Balance orange  41
Nike    pink    44
Nike    pink    38
Adidas  black   39
New Balance white   35
Nike    pink    40
Nike    white   44
Nike    orange  38
Adidas  orange  42
New Balance orange  43
Adidas  pink    39
Adidas  pink    41
Adidas  pink    39
Nike    white   37
Nike    orange  38
Adidas  orange  39
Nike    pink    40
Adidas  white   36
Nike    orange  40
New Balance pink    40
New Balance black   40
New Balance pink    40
Adidas  pink    41
Nike    pink    40
Nike    black   41
Nike    black   39
New Balance white   38
Adidas  black   41
Nike    orange  36
Nike    black   38
New Balance black   40
New Balance pink    40
Adidas  black   42
Adidas  white   40
New Balance orange  38
Nike    pink    41
Adidas  orange  37
Nike    black   44
Adidas  pink    36
Adidas  white   35
Nike    black   38
Nike    pink    42
New Balance black   43
Nike    white   38
New Balance pink    39
Nike    orange  39
New Balance orange  40
New Balance white   44
Adidas  black   42
Nike    black   35
```



### 练习三

写一个阶乘函数fac(n)，用time模块统计当n分别为：1，2，3，...,100时所用的时间，并逐行输出



### 练习四

用turtle模块绘制一个边长为100的五角星

![image-20201016150815289](imgs/image-20201016150815289.png)



## 参考答案



### 练习一

```python
# -*- coding: utf-8 -*-

def main():
    encrypt_sen = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."
    
    # 解密逻辑为：按字母次序往前加2，例如 a -> c, b -> d, c -> e
    tran_map = {
        # 这几个字母加2后需要往前循环，单独处理
        'y': 'a', 'z': 'b', 'Y': 'A', 'Z': 'B'
    }
    for alnum in range(24):
        tran_map[chr(97 + alnum)] = chr(97 + alnum + 2)   # 小写字母
        tran_map[chr(65 + alnum)] = chr(65 + alnum + 2)   # 大写字母

    decrypt_sen = []
    for s in encrypt_sen:
        # 字母进行转换，其它不需要转换
        decrypt_sen.append(tran_map[s]) if s.isalnum() else decrypt_sen.append(s)
    print(''.join(decrypt_sen))


if __name__ == '__main__':
    main()
```





### 练习二

```python
# -*- coding: utf-8 -*-

def main():
    # 获取文件内容，这里假设 shoes.txt 与当前代码在同一目录下
    with open('shoes.txt', 'r') as f:
        lines = f.readlines()
    result = []
    for line in lines:
        # 将本行内容转换为列表
        words = line.strip().split(" ")
        result.append({
            "brand": words[0],   # 品牌
            "color": "black",
            "size": words[-1]    # 尺寸
        })
    print(result)

if __name__ == '__main__':
    main()

```





### 练习三

```python
# -*- coding: utf-8 -*-

import time

def fac(n):   # 计算阶乘
    result = 1
    while n > 0:
        result *= n
        n -= 1
    return result
    
def main(n):
    for x in range(1, n + 1):
        start = time.time()   # 执行之前时间
        result = fac(x)
        end = time.time()   # 执行之后时间
        print('当 n = {} 时，结果为：{}，耗时为：{} 秒'.format(x, result, end - start))

if __name__ == '__main__':
    main(100)
```



### 练习四

```python
# -*- coding: utf-8 -*-

import turtle

def main():
    p = turtle.Pen()
    p.pencolor('blue')
    p.pensize('4')
    for x in range(1, 6):
        p.forward(100)
        p.left(216)     # 先向右直行，然后左转216°(正五角星度数36°)

if __name__ == '__main__':
    main()
```

