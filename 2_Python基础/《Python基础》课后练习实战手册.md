# 《Python基础》课后练习实战手册

## 1. 编写python代码统计输入的字符串中字母和数字的个数，并输出

> 开发环境：上一节课已搭建好的本地开发环境

### 1.1 编写代码并验证准确性

基于python脚本语言特性，我们可以在当前环境下的任意.py文件中编写代码并运行获得结果。或者使用控制台直接输入程序运行进行验证。

参考代码：

```python
# 获取键盘输入
s = input() 
d = {"DIGITS": 0, "LETTERS": 0}
for c in s:
    if c.isdigit():
        d["DIGITS"] += 1
    elif c.isalpha():
        d["LETTERS"] += 1
    else:
        pass
print("LETTERS", d["LETTERS"])
print("DIGITS", d["DIGITS"])
```

### 1.2 添加URL并本地访问测试

作业完成要求为以下URL格式：

`https://ce.bktencent.com/o/{your_appid}/python/homework1/`

与上一节正式环境的helloworld的URL对比

`https://ce.bktencent.com/o/{your_appid}/helloworld/`

我们可以看到，区别在于 `helloworld/` 和 `python/homework1/`，故在`home_application` 下的 `urls.py` 中添加路由 `python/homework1/` 即可。

按照上节课的步骤：

1. 于 `home_application/templates/home_application` 下创建html文件`python_homework1.html` (当前名字只作为当前实战手册样例，无特殊含义)

![avater](imgs/1.png)

2. 在 `home_application/views.py` 中添加相应的函数来返回当前界面

![avater](imgs/2.png)

3. 在 `home_application/urls.py` 中添加相应的路由来匹配上一步的函数

![avater](imgs/3.png)

4. 在浏览器中查看效果

![avater](imgs/4.png)

### 1.3 在浏览器中展示源代码

在上一步中，我们已经成功调通了相应的路由，这时只要将源代码放入上一步的html文件中就可以将其展示在浏览器页面上。为了达成这个目的，我们可以使用html的`<pre>`标签。

>`<pre>` 标签可定义预格式化的文本。
>
>被包围在 `<pre>` 标签 元素中的文本通常会保留空格和换行符。而文本也会呈现为等宽字体。

1. 修改python_homework1.html文件，复制作业一的代码段

![avater](imgs/5.png)

2. 在浏览器中查看效果

![avater](imgs/6.png)



## 2. 编写Python代码判断用户输入的两个单词是否字母相同，且出现的次数也一样

### 2.1 编写代码并验证准确性

参考代码：

```python
def is_anagram(str1, str2):
    list_str1 = list(str1)
    list_str1.sort()
    list_str2 = list(str2)
    list_str2.sort()

    return list_str1 == list_str2


print(is_anagram('anagram', 'nagaram'))
print(is_anagram('cat', 'rat'))
```

### 2.2 添加URL并本地访问测试

由于作业二与作业一的要求一致，此处不再详细进行每一步讲解，同学们可根据作业一中讲解自行完成作业二的界面展示。



## 3. 一个机器人从原点（0，0）出发，可以进行上（UP）、下（DOWN）、左（LEFT）、右（RIGHT）移动，移动的方向和距离由用户输入，输入STOP表示停止输入。请编写Python代码计算机器人停止移动时与原点的距离(当距离不是整数时，使用最接近的整数)。

### 3.1 编写代码并验证准确性

参考代码：

```python
import math

pos = [0, 0]
while True:
    s = input()
    if not s:
        break
    if s == "STOP":
        break
    movement = s.split(" ")
    direction = movement[0]
    steps = int(movement[1])
    if direction == "UP":
        pos[0] += steps
    elif direction == "DOWN":
        pos[0] -= steps
    elif direction == "LEFT":
        pos[1] -= steps
    elif direction == "RIGHT":
        pos[1] += steps
    else:
        pass

print(int(round(math.sqrt(pos[1] ** 2 + pos[0] ** 2))))

```

### 3.2 添加URL并本地访问测试

由于作业三与作业一的要求一致，此处不再详细进行每一步讲解，同学们可根据作业一中讲解自行完成作业三的界面展示。



## 4. 部署到正式环境

部署到正式环境与上一次部署完全相同，只用将修改完的代码commit后push到远程仓库进行部署即可，这里不再重复赘述，仅展示当前实战样例的最终代码结构和部署后效果图。

最终 `home_application` 的代码结构如下 (仅为实战手册样例代码结构，仅供参考，并非强制要求)**

![avater](imgs/7.png)

最终效果图

![avater](imgs/8.png)

![avater](imgs/9.png)

![avater](imgs/10.png)

