

# Windows 下输出 Helloworld

> 开发语言：`Python 3.6.X`
>
> 操作系统：`Windows 10`

## 1. 环境准备

安装 Python 3.6.X、准备数据库、创建应用和准备代码仓库地址、虚拟环境（可选）

### 1.1 安装 Python 3.6.X

下载 Python 3.6.X 的 [Windows x86-64 executable installer](https://www.python.org/downloads/)（现在一般都是 64 位系统）

勾选 `Add Python 3.6 to PATH`，按提示安装即可。

![image-20201012200133118](media/image-20201012200133118.png)



验证 Python 和 pip 是否已经安装成功（安装包会自动安装 `pip`）

![image-20201012200143857](media/image-20201012200143857.png)

### 1.2 部署 MySQL

- [官网下载 MySQL](https://dev.mysql.com/downloads/mysql/) ,选择`.msi`文件进行下载 ，按提示安装即可。
- 验证 MySQL 是否已经安装成功，在cmd执行以下命令

```
 mysql --version
```



### 1.3 准备虚拟环境 (可选：只有一个开发项目的可忽略)

- [修改 pip 源](https://pip.pypa.io/en/stable/user_guide/#config-file) (国内镜像)

  在 `C:\Users\{YOUR_USERNAME}\AppData\Roaming` (Windows 10 默认隐藏 `AppData` 目录，需先去掉隐藏)目录中创建 `pip` 文件夹，在 pip 文件夹下创建 `pip.ini`

  ```bash
  [global]
  trusted-host = pypi.tuna.tsinghua.edu.cn
  index-url = https://pypi.tuna.tsinghua.edu.cn/simple/
  ```

- 安装 [pipenv](https://zhuanlan.zhihu.com/p/37581807)

```bash
pip3 install pipenv
```

- 创建虚拟环境

```bash
pipenv install
```

- 激活虚拟环境

```bash
pipenv shell
```





## 2. 创建应用和准备 Git 仓库

### 2.1 创建Git代码仓库

创建应用前，需提前准备Git代码仓库，推荐使用 [gitee](https://gitee.com/) 的**私有仓库**（使用公开仓库可能会导致秘钥等敏感信息泄露，请注意！）

![image-20201012164453668](media/image-20201012164453668.png)



![image-20210925171627622](media/image-20210925171627622.png)





仓库创建完成后，可以看到仓库的首页的配置向导

![image-20210925171650275](media/image-20210925171650275.png)



### 2.2 将代码仓库同步到本地

仓库首页给出的初始化命令如下。如果是使用 Git Bash 作为命令行工具，则可直接执行成功。如果是用windows默认的cmd，会报 `mkdir`, `touch` 等命令不存在的问题，我们需要手动处理。

```shell
# 在当前工作目录下创建 bk_demo 的文件夹。若 mkdir 命令不存在，则需要手动创建文件夹
mkdir bk_demo

# 进入 bk_demo 目录
cd bk_demo

# 将 bk_demo 目录初始化为 git 仓库
git init

# 创建一个名为 README.md 的文件。若 touch 命令不存在，则需要手动创建该文件
touch README.md

# 将 README.md 这个文件的改动添加到暂存区
git add README.md

# 将 README.md 这个文件的改动提交到本地版本库
git commit -m "first commit"

# 设置远端仓库的地址（xxx）请替换为实际的用户名
git remote add origin https://gitee.com/xxx/bk_demo.git

# 将本地 master 分支的更新，推送到远程主机
git push -u origin master
```

`git push` 命令可能会要求输入Git用户名和密码，如实输入即可



### 2.3 在开发者中心创建应用

访问蓝鲸平台：`https://ce.bktencent.com/` ，进入 `开发者中心` - `应用创建` ，填写上一步获取的 `git` 仓库地址和账号

![1](media/1.png)

### 2.4 下载初始化框架

创建完成后，点击下载应用初始框架代码

代码包压缩格式为 `tar.gz`，请自行安装解压工具进行解压

![1](media/4.png)



将解压后的初始化框架代码，拷贝到仓库目录下

![image-20210925174909672](media/image-20210925174909672.png)



最后提交代码到线上的代码仓库

```shell
# 将代码提交到暂存区
git add .

# 将代码提交到本地的 master 分支
git commit -m "feature: 提交框架代码"

# 将代码提交到远端的 master 分支
git push
```



即可在线上的gitee上查看到刚才提交的代码

![1](media/10.png)

## 3. 初始化开发框架

### 3.1 修改配置

在开发者中心查询到 `APP_CODE` (应用ID) 和 `SECRET_KEY` (应用TOKEN) ，然后使用shell相关的命令进行关于以下环境变量的配置。

```shell
export BKPAAS_APP_ID="APP_CODE"
export BKPAAS_APP_SECRET="SECRET_KEY"
export BKPAAS_MAJOR_VERSION="3"
export BK_PAAS2_URL="https://ce.bktencent.com"
export BK_COMPONENT_API_URL="https://bkapi.ce.bktencent.com"
```

或者可以在Pycharm中进行相应的配置如图下所示：

![1](media/3.png)

### 3.2 安装开发框架依赖包

```bash
pip3 install -r requirements.txt
```

### 3.3 创建和初始化数据库

在CMD上执行以下命令，进入 MySQL 控制台。用户名和密码在安装 MySQL 时已确定

```
mysql -u用户名 -p密码
```

在 MySQL 控制台中执行以下命令

```bash
CREATE DATABASE `{APP_CODE}` default charset utf8 COLLATE utf8_general_ci;
```

> 如果 {APP_CODE} 中包含连接符 (-)，需要使用反引号( ` )转译，否则会报错

> `ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '-blueking' at line 1`

并修改 `config/dev.py` 中 `DATABASES` 配置项

![1](media/8.png)

- 初始化本地数据库(在工程根目录下)

```bash
python manage.py migrate
```

### 3.4 启动本地项目

- 先修改本地 hosts。在 `C:\Windows\System32\drivers\etc\hosts` 文件增加一行（注意要以管理员身份打开，否则无法保存）

```
127.0.0.1 appdev.ce.bktencent.com
```

- 启动项目

```bash
python manage.py runserver appdev.ce.bktencent.com:8000
```

- 本地访问
用浏览器访问 `http://appdev.ce.bktencent.com:8000` ，就可以看到开发框架的实例页面

![image-20201012200207700](media/image-20201012200207700.png)

## 4. 编写 Helloworld 页面

- 修改视图文件 `home_application/views.py`，在文件末尾添加以下函数

```python
def helloworld(request):
    return render(request, 'home_application/helloworld.html')

```

- 修改路由文件 `home_application/urls.py`
```python
urlpatterns = (
    url(r'^$', views.home),
    url(r'^dev-guide/$', views.dev_guide),
    url(r'^contact/$', views.contact),
    # 添加 helloworld 路由
    url(r'^helloworld/$', views.helloworld),
)
```

- 新增html文件 `home_application/templates/home_application/helloworld.html`，内容如下

```
helloworld
```

- 重新执行 `python manage.py runserver appdev.ce.bktencent.com:8000` ，让新代码生效。若之前的 server 进程已经在执行 ，可在命令行 `Ctrl` + `C` 直接终止
- 在浏览器访问 `http://appdev.ce.bktencent.com:8000/helloworld/` ,将显示 `helloworld` 文本

## 5. 部署到线上环境

### 5.1 推送本地代码到线上仓库

在cmd中进入到代码仓库根目录，执行以下命令

```shell
# 将当前目录所有文件添加到暂存区
git add .

# 将改动提交到本地版本库
git commit -m "feature: add helloworld"

# 将本地 master 分支的更新，推送到远程主机
git push
```

### 5.2 线上部署

1.访问开发者中心`https://bkpaas.ce.bktencent.com/developer-center`，找到需要部署的SaaS，点击“部署”

![1](media/2.png)

2.进入到预发布环境部署页面之后，点击“部署”，应用将立即开始部署到预发布环境。稍等片刻后，点击上方“马上访问”即可访问测试环境页面。

![1](media/5.png)

3.尝试部署到生产环境当中

![1](media/6.png)

4.点击尝试访问，观察是否符合预期

![1](media/7.png)